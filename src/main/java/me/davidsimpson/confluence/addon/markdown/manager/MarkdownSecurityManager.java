package me.davidsimpson.confluence.addon.markdown.manager;

import me.davidsimpson.confluence.addon.markdown.bean.MarkdownSecurity;

/**
 * User: david
 * Date: 09/09/2013
 */
public interface MarkdownSecurityManager
{
    public static final String ADD_ON_KEY = "me.davidsimpson.confluence.addon.markdown";

    public void setSecurity(MarkdownSecurity markdownSecurity);

    public MarkdownSecurity getSecurity();
}
