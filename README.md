# Markdown for Confluence

- Finally, you can write your Confluence pages in markdown!
- In fact, you can mix and match the superb Confluence editor and Markdown by using the Markdown macro
- There's even a Markdown Blueprint for quickly creating a page with the Markdown macro already inserted.

# → [Download Markdown for Confluence 1.2](https://bitbucket.org/dvdsmpsn/markdown-for-confluence/downloads/markdown-for-confluence-1.2.jar)

---


## Security

For security, embedded HTML is escaped by default, but you can turn HTML escaping off if you really want.


## Support

For support and feature requests:

- Ask a question on [Stackoverflow](http://stackoverflow.com/) using the tag `markdown-for-confluence`
- [Fill out the contact form](http://davidsimpson.me/contact-me/)
- [Email](mailto:david@davidsimpson.me?subject=Markdown%20for%20Confluence%3A%20Support)


## License

Markdown for Confluence is licensed using the [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0).


## Testing

Tested and working for most examples in [uncc / markdown-examples](https://github.com/uncc/markdown-examples)

### Test Passed

- [Blockquotes](https://raw.github.com/uncc/markdown-examples/master/blockquotes.md)
- [Code](https://raw.github.com/uncc/markdown-examples/master/code.md)
- [Emphasis](https://raw.github.com/uncc/markdown-examples/master/emphasis.md)
- [Headers](https://raw.github.com/uncc/markdown-examples/master/empheadershasis.md)
- [Images](https://raw.github.com/uncc/markdown-examples/master/images.md)
- [Links](https://raw.github.com/uncc/markdown-examples/master/links.md)
- [Lists](https://raw.github.com/uncc/markdown-examples/master/lists.md)
- [Paragraphs](https://raw.github.com/uncc/markdown-examples/master/paragraphs.md)

### Test Failed

- [Reference Links](https://raw.github.com/uncc/markdown-examples/master/reference-links.md)
- [Tables](https://raw.github.com/uncc/markdown-examples/master/tables.md)


## Credits

-	Based on [PegDown](https://github.com/sirthias/pegdown), a pure-Java Markdown processor
	-	PegDown uses [Parbolied](http://www.parboiled.org/), a mixed Java/Scala library providing for lightweight and easy-to-use, yet powerful and elegant parsing of arbitrary input text based on [Parsing expression grammars](http://en.wikipedia.org/wiki/Parsing_expression_grammar) (PEGs)
    -	PegDown and Parboiled are both licensed using the [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
-	The "[Markdown Mark](https://github.com/dcurtis/markdown-mark)" by Dustin Curtis
    -	Licensed under [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/deed.en)